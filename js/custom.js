/* //Animated-Scroll-Bar */

jQuery('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 1,
            nav: true,
            loop: false
        }
    }
})

/* Tooltip For Accessrires oages */
jQuery(document).ready(function () {
    jQuery('[data-toggle="tooltip"]').tooltip();

    jQuery('header .navbar ul.nav-pills li a[data-link]').click(function () {

        jQuery(this).attr("href", "http://www.google.com");



    });

});



/* Custom-Scroll Bar */
(function ($) {
    jQuery(window).on("load", function () {

        jQuery.mCustomScrollbar.defaults.theme = "light-2"; //set "light-2" as the default theme

        jQuery(".tabScroll").mCustomScrollbar({
            axis: "x",
            advanced: {
                autoExpandHorizontalScroll: true
            }
        });

    });
})(jQuery);
/* First-Cap*/
jQuery('.firstCap').on('keypress', function (event) {
    var $this = $(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    $(this).val(FLC + con);
});
/*====----=====*/

/*  Nabbar-Mega-menu */
(function ($) {
    jQuery(function () {
        jQuery(document).off('click.bs.tab.data-api', 'header [data-hover="tab-hover"]');
        jQuery(document).on('mouseenter.bs.tab.data-api', 'header [data-toggle="tab-hover"], header [data-hover="tab-hover"]', function () {
            jQuery(this).tab('show');
        });
    });
})(jQuery); /*  //Nabbar-Mega-menu */

jQuery(function () {
  jQuery('[data-toggle="tooltip"]').tooltip()
})